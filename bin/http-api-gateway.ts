#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { HttpApiGatewayStack } from '../lib/http-api-gateway-stack';

const app = new cdk.App();
new HttpApiGatewayStack(app, 'HttpApiGatewayStack');
