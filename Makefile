define HELP

Usage:

make build              - Build the HTTP API Gateway CloudFormation template
make deploy             - Deploy the HTTP API Gateway CloudFormation stack
make destroy            - Destroy the HTTP API Gateway CloudFormation stack
make base-requirements  - Install base npm and pip dependencies
make save-requirements  - Save Python dependencies for upload to AWS
make test-requirements  - Install npm and pip dependencies for tests
make test-unit          - Execute unit tests for Lambda function code

endef

export HELP

all help:
	@echo "$$HELP"

build: save-requirements
	npm run build

deploy:
	cdk deploy --require-approval=never

destroy:
	cdk destroy -f

base-requirements:
	npm i
	pip install -r requirements/base.txt

save-requirements:
	find src/order_service_entry_point/* -not -name "__init__.py" -not -name "app.py" -type d,f | xargs rm -rf
	pip3 install -r requirements/base.txt -t src/order_service_entry_point

test-requirements:
	pip install -r requirements/test.txt

test-unit: test-requirements
	pytest test/unit -v
