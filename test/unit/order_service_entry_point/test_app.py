from http import HTTPStatus
from json import dumps as json_dumps
from unittest.mock import patch
from uuid import uuid4

from pytest import mark

from src.order_service_entry_point import app


@patch.object(app, "generate_response")
@patch.object(app, "handle_request")
def test_lambda_handler_without_exceptions(handle_request, generate_response):
    headers = {"Content-Type": "application/json"}
    is_base_64_encoded = True
    method = "GET"

    request = {
        "isBase64Encoded": is_base_64_encoded,
        "headers": headers,
        "routeKey": f"{method} /orders"
    }

    response_body = json_dumps({"orderId": str(uuid4())})
    response_code = HTTPStatus.OK

    expected_response = {
        "body": response_body,
        "isBase64Encoded": is_base_64_encoded,
        "headers": headers,
        "statusCode": response_code
    }

    handle_request.return_value = (response_code, response_body)
    generate_response.return_value = expected_response

    assert expected_response == app.lambda_handler(request, None)

    handle_request.assert_called_once_with(method)
    generate_response.assert_called_once_with(request, response_body, response_code)


@patch.object(app, "generate_response")
@patch.object(app, "handle_request")
def test_lambda_handler_with_attribute_exception(handle_request, generate_response):
    headers = {"Content-Type": "application/json"}
    is_base_64_encoded = True

    request = {
        "isBase64Encoded": is_base_64_encoded,
        "headers": headers
    }

    response_body = "Route key missing in request"
    response_code = HTTPStatus.BAD_REQUEST

    expected_response = {
        "body": response_body,
        "isBase64Encoded": is_base_64_encoded,
        "headers": headers,
        "statusCode": response_code
    }

    generate_response.return_value = expected_response

    assert expected_response == app.lambda_handler(request, None)

    generate_response.assert_called_once_with(request, response_body, response_code)
    handle_request.assert_not_called()


def test_generate_response():
    response_body = json_dumps({"orderId": str(uuid4())})
    response_code = HTTPStatus.OK

    request = {
        "headers": {"Content-Type": "application/json"},
        "isBase64Encoded": True,
    }

    expected_response = {
        "body": response_body,
        "isBase64Encoded": request["isBase64Encoded"],
        "headers": request["headers"],
        "statusCode": response_code
    }

    assert expected_response == app.generate_response(request, response_body, response_code)


@mark.parametrize(("method", "response_codes"), [
    ("GET", [HTTPStatus.BAD_REQUEST, HTTPStatus.INTERNAL_SERVER_ERROR, HTTPStatus.NOT_FOUND, HTTPStatus.OK]),
    ("POST", [HTTPStatus.BAD_REQUEST, HTTPStatus.CREATED, HTTPStatus.INTERNAL_SERVER_ERROR]),
    ("PUT", [HTTPStatus.BAD_REQUEST, HTTPStatus.INTERNAL_SERVER_ERROR, HTTPStatus.NOT_FOUND, HTTPStatus.OK])
], ids=["GET", "POST", "PUT"])
def test_handle_request(method, response_codes):
    response_code, _ = app.handle_request(method)

    assert response_code in response_codes
